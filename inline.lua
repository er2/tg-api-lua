-- Inline query library
-- (c) Er2 2022 <er2@dismail.de>
-- Zlib License

require 'class'

class 'InlineResult' {
  function(this, opts)
    this.type = opts.type
    this.id = tostring(tonumber(opts.id) or 1)
  end,

  thumb = function(this, url, width, height, mime)
    if this.type == 'audio'
    or this.type == 'voice'
    or this.type == 'game'
    then return this end -- cannot do that

    this.thumb_url = tostring(url)

    if width and height and (
         this.type == 'article'
      or this.type == 'document'
      or this.type == 'contact'
      or this.type == 'location'
      or this.type == 'venue'
    ) then
      this.thumb_width  = tonumber(width)
      this.thumb_height = tonumber(height)
    end

    if mime and (
         this.type == 'gif'
      or this.type == 'mpeg4_gif'
    ) then this.thumb_mime_type = mime end

    return this
  end,

  keyboard = function(this, ...)
    if not this.type
    then return this end

    local k = {}
    for _, v in pairs {...} do
      if type(v) == 'table' then
        table.insert(k, v)
      end
    end
    this.reply_markup = k

    return this
  end,

  messageContent = function(this, content)
    if this.type == 'game'
    or this.type == 'article'
    then this.input_message_content = content
    end
    return this
  end,
}

class 'APIInline' {
  function(this, api)
    this.request = function(self, ...)
      return api:request(...)
    end
  end,

  query = function(id, from, query, offset, type, location)
    return {
      id = tostring(id),
      from = from,
      query = query,
      offset = offset,
      chat_type = type,
      location = location,
    }
  end,

  result = function(type, id, ...)
    type = tostring(type)
    local t = new 'InlineResult' {
      type = type,
      id = id,
    }
    local a = {...}
    if t.type == 'article' then t.title, t.url, t.hide_url, t.description = table.unpack(a)

    elseif t.type == 'photo' then
      t.photo_url, t.photo_width, t.photo_height, t.title, t.description,
      t.caption, t.parse_mode, t.caption_entities
        = table.unpack(a)

    elseif t.type == 'gif' or t.type == 'mpeg4_gif' then
      local url, width, height, duration
      url, width, height, duration, t.title, t.caption, t.parse_mode, t.caption_entities
        = table.unpack(a)

      if t.type == 'gif' then
        t.gif_url, t.gif_width, t.gif_height, t.gif_duration
        = url, width, height, duration
      else
        t.mpeg4_url, t.mpeg4_width, t.mpeg4_height, t.mpeg4_duration
        = url, width, height, duration
      end

    elseif t.type == 'video' then
      t.video_url, t.mime_type, t.title, t.caption, t.parse_mode,
      t.caption_entities, t.video_width, t.video_height, t.video_duration, t.description
        = table.unpack(a)

    elseif t.type == 'audio' or t.type == 'voice' then
      t.title, t.caption, t.parse_mode, t.caption_entities = table.unpack(a, 2)

      if t.type == 'audio' then
        t.audio_url, t.performer, t.audio_duration = a[1], a[6], a[7]
      else
        t.voice_url, t.voice_duration = a[1], a[6]
      end

    elseif t.type == 'document' then
      t.title, t.caption, t.parse_mode, t.caption_entities, t.document_url,
      t.mime_type, t.description = table.unpack(a)

    elseif t.type == 'location' or t.type == 'venue' then
      t.latitude, t.longitude, t.title = table.unpack(a, 1, 3)

      if t.type ~= 'venue' then
        t.horizontal_accurancy, t.live_period, t.heading, t.proximity_alert_radius
        = table.unpack(a, 4, 7)
      else
        t.address, t.foursquare_id, t.foursquare_type, t.google_place_id, t.google_place_type
        = table.unpack(a, 4, 8)
      end

    elseif t.type == 'contact' then
      t.phone_number, t.first_name, t.last_name, t.vcard,
      t.reply_markup, t.input_message_content
        = table.unpack(a)

    elseif t.type == 'game' then t.game_short_name = a[1]
    end

    return t
  end,

  answer = function(this, id, results, caches, personal, nextOffset, pmText, pmParam)
    if results.id
    then results = {results} end -- single
    return this:request('answerInlineQuery', {
      inline_query_id = id,
      results = results,
      cache_time = caches,
      is_personal = personal,
      next_offset = nextOffset,
      switch_pm_text = pmText,
      switch_pm_param = pmParam,
    })
  end,
}

